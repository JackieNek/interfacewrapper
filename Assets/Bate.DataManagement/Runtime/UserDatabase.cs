﻿namespace Bate.DataManagement
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    public static class UserDatabase
    {
        private static Dictionary<string, PropertyInfo> members = Member();
        private static HashSet<string> dirty = new();

        public static JsonSerializerSettings Settings = new()
        {
            Formatting = Formatting.Indented,
            TypeNameHandling = TypeNameHandling.All,
            TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Simple
        };

        public static JsonSerializer Serializer = JsonSerializer.CreateDefault(Settings);


        public static void SetDirty(Token token)
        {
            dirty.Add(token);
        }

        public static JObject LoadDirty()
        {
            var jObject = new JObject();
            foreach (var key in dirty)
                jObject.Add(key, JToken.FromObject(members[key].GetValue(null), Serializer));
            return jObject;
        }

        public static Dictionary<string, PropertyInfo> Member()
        {
            var member = new Dictionary<string, PropertyInfo>();

            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                foreach (var type in assembly.GetTypes())
                {
                    var properties = type.GetProperties();
                    foreach (var property in properties)
                    {
                        var userFieldAttribute = property.GetCustomAttribute<UserPropertyAttribute>();
                        if (userFieldAttribute != null) member.Add(new Token(type, property.Name), property);
                    }
                }
            }

            return member;
        }

        public static JObject LoadJObject()
        {
            var jObject = new JObject();

            foreach (var (key, property) in members)
                jObject.Add(key, JToken.FromObject(property.GetValue(null), Serializer));

            return jObject;
        }

        public static void SaveJObject(JObject jObject)
        {
            foreach (var (key, property) in members)
                if (jObject.TryGetValue(key, out var jToken))
                    property.SetValue(null, jToken.ToObject(property.PropertyType, Serializer));
        }
    }
}