﻿using UnityEngine;

namespace Bate.DataManagement
{
    using System;

    public struct Token
    {
        private string name;

        public Token(Type type, string propertyName)
        {
            name = $"{type}.{propertyName}".Replace(".", "");
        }

        public static implicit operator string(Token token)
        {
            return token.name;
        }
    }


    
    public interface IWeapon
    {
        
    }

    [Serializable]
    public struct Gun : IWeapon
    {
        [SerializeField] private int damage;
    }
}