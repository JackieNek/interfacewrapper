namespace Bate.DataManagement
{
    using System;

    [AttributeUsage(AttributeTargets.Property)]
    public class UserPropertyAttribute : Attribute { }
}