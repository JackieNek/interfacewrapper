﻿// namespace Bate.DataManagement.Editor
// {
//     using System.Collections.Generic;
//     using System.Reflection;
//     using Sirenix.OdinInspector;
//     using Sirenix.OdinInspector.Editor;
//     using Sirenix.Serialization;
//     using UnityEditor;
//
//     public class UserEditorWindow : OdinEditorWindow
//     {
//         [OdinSerialize, DictionaryDrawerSettings(IsReadOnly = true, KeyLabel = "Property Path"), LabelText("User Data")]
//         Dictionary<string, object> dictionary;
//         private Dictionary<string, PropertyInfo> properties;
//
//         [ButtonGroup]
//         private void Save()
//         {
//             foreach (var (key, value) in dictionary)
//             {
//                 properties[key].SetValue(null, value);
//             }
//         }
//
//         [ButtonGroup]
//         private void Reload()
//         {
//             dictionary = new Dictionary<string, object>();
//             properties = new Dictionary<string, PropertyInfo>();
//             foreach (var (key, value) in UserDatabase.Member())
//             {
//                 dictionary.Add(key, value.GetValue(null));
//                 properties.Add(key, value);
//             }
//         }
//
//         protected override void OnEnable()
//         {
//             base.OnEnable();
//             Reload();
//         }
//
//         [MenuItem("Tools/User/Data Management")]
//         public static void MenuItem()
//         {
//             GetWindow<UserEditorWindow>("User Database").Show();
//         }
//     }
// }