using System;
using System.Collections.Generic;
using Bate.DataManagement;
using InterfaceWrapper.Runtime;
using JSerializer.Runtime;
using UnityEngine;

[ExecuteInEditMode]
public class MyScript : SerializedMonoBehaviour
{
    public Wrapper<IWeapon> Weapon;
    public List<Wrapper<IWeapon>> ListWeapon;
    public Wrapper<IWeapon>[] ArrayWeapon;

    [ContextMenu("Exe")]
    public void Exe()
    {

    }
    
    // [ContextMenu("Log")]
    // public void Log()
    // {
    //     Debug.Log("Weapon" + Weapon);
    //     Debug.Log("OzeWeapon" + OzeWeapon);
    //     Debug.Log("NullWeapon" + NullWeapon);
    // }
}

public interface ICharacter<T>
{
    
}


[Serializable]
public class Tanker : ICharacter<bool>
{
    public float xoet;
}

[Serializable]
public class Supporter : ICharacter<bool>
{
    public float xet;

}

[Serializable]
public class Adc : MonoBehaviour, ICharacter<bool>
{
    
}

