﻿using System;
using Bate.DataManagement;
using JSerializer.Runtime;
using UnityEngine;

[Serializable]
public class Shield : IWeapon, ISerializedObject
{
    [JSerializeField] public IWeapon WrapperWeapon;
    [SerializeField] private int value;
}