﻿using System.Collections.Generic;
using InterfaceWrapper.Runtime;
using UnityEditor;
using UnityEngine;

public class MyWindow : EditorWindow
{
    [MenuItem("Tools/My Window")]
    public static void GetWindow()
    {
        GetWindow<MyWindow>().Show();
    }
    
    [SerializeReference, SubclassSelector] private List<INode> nodes;

    private Editor editor;
    private MyScriptableObject scriptableObject;

    private void OnEnable()
    {
        scriptableObject = CreateInstance<MyScriptableObject>();
        editor = Editor.CreateEditor(scriptableObject);
    }

    private void OnGUI()
    {
        if (ActiveEditorTracker.HasCustomEditor(scriptableObject))
        {
            editor.OnInspectorGUI();
        }
        else
        {
            editor.DrawDefaultInspector();
        }
    }
}