﻿using System.Collections.Generic;
using InterfaceWrapper.Runtime;
using UnityEngine;

[CreateAssetMenu(menuName = "Create MyScriptableObject", fileName = "MyScriptableObject", order = 0)]
public class MyScriptableObject : ScriptableObject
{
    [SerializeReference, SubclassSelector] private List<INode> nodes;
}