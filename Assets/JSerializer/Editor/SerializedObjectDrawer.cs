﻿#if JINSPECTOR
using System.Collections.Generic;
using JSerializer.Runtime;
using UnityEditor;
using UnityEngine;

namespace JSerializer.Editor
{
    [CustomPropertyDrawer(typeof(JSerializedObject))]
    public class SerializedObjectDrawer : PropertyDrawer
    {
        private Dictionary<string, GUIContent> guiContents = new();

        private GUIContent GUIContent(string name)
        {
            if (guiContents.TryGetValue(name, out var guiContent)) return guiContent;

            guiContent = new GUIContent(ObjectNames.NicifyVariableName(name));
            guiContents.Add(name, guiContent);

            return guiContent;
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            var entriesProperty = property.FindPropertyRelative("entries");
            var height = 0f;
            for (int i = 0; i < entriesProperty.arraySize; i++)
            {
                var entryProperty = entriesProperty.GetArrayElementAtIndex(i);
                height += EditorGUI.GetPropertyHeight(entryProperty, true);
            }

            return height;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var entriesProperty = property.FindPropertyRelative("entries");
            for (int i = 0; i < entriesProperty.arraySize; i++)
            {
                var entryProperty = entriesProperty.GetArrayElementAtIndex(i);
                var entryPosition = new Rect(position)
                {
                    height = EditorGUI.GetPropertyHeight(entryProperty, true),
                };
                position.y += entryPosition.height;
                EditorGUI.PropertyField(entryPosition, entryProperty, true);
            }
        }
    }
}
#endif