﻿#if JINSPECTOR
using JSerializer.Runtime;
using UnityEditor;
using UnityEngine;

namespace JSerializer.Editor
{
    [CustomPropertyDrawer(typeof(SystemNode))]
    public class SystemNodeDrawer : PropertyDrawer
    {
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            var objectValueProperty = property.FindPropertyRelative("objectValue");
            return EditorGUI.GetPropertyHeight(objectValueProperty);
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var objectValueProperty = property.FindPropertyRelative("objectValue");
            EditorGUI.PropertyField(position, objectValueProperty, label, true);
        }
    }
}
#endif
