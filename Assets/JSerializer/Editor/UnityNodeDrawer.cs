﻿#if JINSPECTOR
using System;
using JSerializer.Runtime;
using UnityEditor;
using UnityEngine;

namespace JSerializer.Editor
{
    [CustomPropertyDrawer(typeof(UnityNode))]
    public class UnityNodeDrawer : PropertyDrawer
    {
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return (property.isExpanded ? 2 : 1) * (base.GetPropertyHeight(property, label) + EditorGUIUtility.standardVerticalSpacing);
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var objectValueProperty = property.FindPropertyRelative("objectValue");
            var foldoutPosition = new Rect(position)
            {
                width = EditorGUIUtility.labelWidth,
                height = EditorGUIUtility.singleLineHeight,
            };

            property.isExpanded = EditorGUI.Foldout(foldoutPosition, property.isExpanded, label, true);
            if (property.isExpanded)
            {
                var fieldPosition = new Rect(position)
                {
                    y = position.y + EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing,
                    height = EditorGUI.GetPropertyHeight(objectValueProperty),
                };

                var propertyPath = property.propertyPath;
                var entryPath = propertyPath.Substring(0, propertyPath.Length - 5);
                var type = (Type)((Entry)objectValueProperty.serializedObject.FindProperty(entryPath).boxedValue)
                    .TargetType;
                objectValueProperty.objectReferenceValue = EditorGUI.ObjectField(fieldPosition, new GUIContent(" "),
                    objectValueProperty.objectReferenceValue, type, true);
            }
        }
    }
}
#endif