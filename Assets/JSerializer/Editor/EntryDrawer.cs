﻿#if JINSPECTOR
using System;
using JSerializer.Runtime;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace JSerializer.Editor
{
    [CustomPropertyDrawer(typeof(Entry))]
    public class EntryDrawer : PropertyDrawer
    {
        private Type unityObjectType = typeof(Object);
        private Type serializedObject = typeof(ISerializedObject);

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            var nodeProperty = property.FindPropertyRelative(nameof(Entry.Node));
            return EditorGUI.GetPropertyHeight(nodeProperty);
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var stringProperty = property.FindPropertyRelative(nameof(Entry.Name));
            var nodeProperty = property.FindPropertyRelative(nameof(Entry.Node));
            var targetTypeProperty = property.FindPropertyRelative(nameof(Entry.TargetType));
            var rootTypeProperty = property.FindPropertyRelative(nameof(Entry.RootType));

            label.text = ObjectNames.NicifyVariableName(stringProperty.stringValue);
            var rootType = (Type)(EntryType)rootTypeProperty.boxedValue;
            var targetType = (Type)(EntryType)targetTypeProperty.boxedValue;


            var types = TypeCacheUtility.Children(rootType);
            var typeIndex = Mathf.Max(0, Array.IndexOf(types, targetType));
            if (targetType == null)
            {
                typeIndex = EditorGUI.Popup(position, label, typeIndex,
                    GUIContentCache.Children((EntryType)rootTypeProperty.boxedValue));
            }
            else
            {
                var popupPosition = new Rect(position)
                {
                    x = position.x + EditorGUIUtility.labelWidth,
                    height = EditorGUIUtility.singleLineHeight,
                };
                typeIndex = EditorGUI.Popup(popupPosition, typeIndex, GUIContentCache.Children(rootType));
                EditorGUI.PropertyField(position, nodeProperty, label, true);
            }

            if (targetType != types[typeIndex])
            {
                targetTypeProperty.boxedValue = (EntryType)types[typeIndex];
                nodeProperty.boxedValue = NewNode(types[typeIndex]);
            }
        }

        private object NewNode(Type valueType)
        {
            if (valueType == null) return null;
            
            if (unityObjectType.IsAssignableFrom(valueType))
            {
                return new UnityNode();
            }
            
            if (serializedObject.IsAssignableFrom(valueType))
            {
                return new SerializedNode
                {
                    Value = Activator.CreateInstance(valueType),
                };
            }

            return new SystemNode
            {
                Value = Activator.CreateInstance(valueType),
            };
        }
    }
}
#endif
