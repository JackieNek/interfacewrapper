﻿using UnityEngine;

namespace JSerializer.Runtime
{
    public abstract class SerializedMonoBehaviour : MonoBehaviour, ISerializationCallbackReceiver
    {
        [SerializeField] private JSerializedObject jSerializedObject = new();

        void ISerializationCallbackReceiver.OnBeforeSerialize()
        {
            jSerializedObject.OnBeforeSerialize(this);
        }

        void ISerializationCallbackReceiver.OnAfterDeserialize()
        {
            jSerializedObject.OnAfterDeserialize(this);
        }
    }
}