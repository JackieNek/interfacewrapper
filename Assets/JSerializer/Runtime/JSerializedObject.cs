﻿namespace JSerializer.Runtime
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;
    using Object = UnityEngine.Object;

    [Serializable]
    public class JSerializedObject
    {
        private Dictionary<string, INode> mapNode = new();
        [SerializeField] private List<Entry> entries = new();

        public void OnBeforeSerialize(object context)
        {
            var fieldInfos = TypeCacheUtility.JSerializedField(context.GetType());

            // convert field to node
            foreach (var fieldInfo in fieldInfos)
            {
                var fieldValue = fieldInfo.GetValue(context);
                if (fieldValue == null)
                {
                    if (mapNode.TryAdd(fieldInfo.Name, null))
                    {
                        mapNode[fieldInfo.Name] = null;
                    }

                    continue;
                }

                if (mapNode.TryGetValue(fieldInfo.Name, out var node))
                {
                    if (UpdateNode(fieldValue, node))
                    {
                        mapNode[fieldInfo.Name] = NewNode(fieldValue);
                    }
                    else
                    {
                        node.Value = fieldValue;
                    }
                }
                else
                {
                    mapNode.Add(fieldInfo.Name, NewNode(fieldInfo.GetValue(context)));
                }
            }

            var fieldDetectTypes = TypeCacheUtility.FieldTypeDetect(context.GetType());
#if UNITY_EDITOR && JINSPECTOR
            var backup = new Dictionary<string, EntryType>();
            foreach (var entry in entries)
            {
                backup.Add(entry.Name, entry.TargetType);
            }

#endif

            // convert node to entry
            entries = mapNode.Where(pair => fieldDetectTypes.ContainsKey(pair.Key)).Select(pair =>
            {
                var entry = new Entry
                {
                    Name = pair.Key,
                    Node = pair.Value,
                };

#if UNITY_EDITOR && JINSPECTOR
                entry.RootType = fieldDetectTypes[pair.Key];
                entry.TargetType = backup.TryGetValue(pair.Key, out var entryType) ? entryType : new EntryType();
#endif

                return entry;
            }).ToList();
        }

        public void OnAfterDeserialize(object context)
        {
            // convert entry to node;

            mapNode.Clear();

            foreach (var entry in entries)
            {
                mapNode.Add(entry.Name, entry.Node);
            }

            // convert node to field
            var fieldInfos = TypeCacheUtility.JSerializedField(context.GetType());
            foreach (var fieldInfo in fieldInfos)
            {
                if (mapNode.TryGetValue(fieldInfo.Name, out var node) && node is { Value: not null })
                {
                    if (node.Value is Object unityObject)
                    {
                        if (unityObject != null)
                        {
                            fieldInfo.SetValue(context, unityObject);
                        }
                        else
                        {
                            fieldInfo.SetValue(context, null);
                        }
                    }
                    else
                        fieldInfo.SetValue(context, node.Value);
                }
                else
                {
                    fieldInfo.SetValue(context, null);
                }
            }
        }

        private bool UpdateNode(object value, INode node)
        {
            if (node == null) return true;
            if (value is Object && node is UnityNode)
                return false;
            if (value is not Object && node is SystemNode)
                return false;
            return true;
        }

        private INode NewNode(object value)
        {
            if (value is Object)
            {
                return new UnityNode();
            }

            if (value is ISerializedObject)
            {
                return new SerializedNode();
            }


            return new SystemNode();
        }
    }

    public interface ISerializedObject
    {
    }
}