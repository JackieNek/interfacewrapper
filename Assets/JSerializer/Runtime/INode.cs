﻿namespace JSerializer.Runtime
{
    public interface INode
    {
        public object Value { get; set; }
    }
}