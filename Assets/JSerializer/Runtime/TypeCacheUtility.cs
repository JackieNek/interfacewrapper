﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace JSerializer.Runtime
{
    public class TypeCacheUtility
    {
        private static Dictionary<Type, FieldInfo[]> fieldList = new Dictionary<Type, FieldInfo[]>();

        private static Dictionary<Type, Dictionary<string, Type>> fieldTypeDetect =
            new Dictionary<Type, Dictionary<string, Type>>();

        private static Dictionary<Type, Type[]> typesMap = new Dictionary<Type, Type[]>();

        public static Type[] Children(Type parent)
        {
            if (typesMap.TryGetValue(parent, out var array))
            {
                return array;
            }

            var list = new List<Type> { null };
            list.AddRange(
                AppDomain.CurrentDomain.GetAssemblies()
                    .SelectMany(assembly => assembly.GetTypes())
                    .Where(t => !t.IsInterface && !t.IsAbstract && !t.IsGenericType)
                    .Where(parent.IsAssignableFrom)
            );
            array = list.ToArray();
            typesMap.Add(parent, array);
            return array;
        }

        public static FieldInfo[] JSerializedField(Type type)
        {
            if (fieldList.TryGetValue(type, out var value))
            {
                return value;
            }

            value = type.GetFields()
                .Where(fieldInfo => fieldInfo.GetCustomAttribute<JSerializeFieldAttribute>() != null).ToArray();
            fieldList.Add(type, value);
            return value;
        }

        public static Dictionary<string, Type> FieldTypeDetect(Type type)
        {
            if (fieldTypeDetect.TryGetValue(type, out var map))
            {
                return map;
            }

            map = new Dictionary<string, Type>();

            foreach (var fieldInfo in JSerializedField(type))
            {
                map.Add(fieldInfo.Name, fieldInfo.FieldType);
            }

            fieldTypeDetect.Add(type, map);

            return map;
        }
    }
}