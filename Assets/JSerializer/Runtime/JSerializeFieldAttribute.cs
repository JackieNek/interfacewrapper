﻿using System;

namespace JSerializer.Runtime
{
    [AttributeUsage(AttributeTargets.Field)]
    public class JSerializeFieldAttribute : Attribute
    {
    }
}