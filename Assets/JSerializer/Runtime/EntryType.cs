﻿using System;

namespace JSerializer.Runtime
{
    [Serializable]
    public class EntryType
    {
        public string Assembly;
        public string Name;

        public static implicit operator EntryType(Type type)
        {
            if (type == null) return new EntryType();
            return new EntryType
            {
                Assembly = type.Assembly.ToString(),
                Name = type.ToString(),
            };
        }

        public static implicit operator Type(EntryType entryType)
        {
            if (entryType == null || string.IsNullOrEmpty(entryType.Assembly) ||
                string.IsNullOrEmpty(entryType.Name)) return null;
            return System.Reflection.Assembly.Load(entryType.Assembly).GetType(entryType.Name);
        }
    }
}