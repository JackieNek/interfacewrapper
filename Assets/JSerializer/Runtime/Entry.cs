﻿using System;
using UnityEngine;

namespace JSerializer.Runtime
{
    [Serializable]
    public class Entry
    {
#if UNITY_EDITOR && JINSPECTOR
        public EntryType RootType;
        public EntryType TargetType;
#endif

        public string Name;
        [SerializeReference] public INode Node;
    }
}