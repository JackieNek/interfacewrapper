﻿using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace JSerializer.Runtime
{
    [Serializable]
    public class UnityNode : INode
    {
        [SerializeField] private Object objectValue;

        public object Value
        {
            get => objectValue;
            set => objectValue = (Object)value;
        }
    }
}