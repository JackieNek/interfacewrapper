﻿using System;
using UnityEngine;

namespace JSerializer.Runtime
{
    public class SystemNode : INode
    {
        [SerializeReference] private object objectValue;

        public object Value
        {
            get => objectValue;
            set => objectValue = value;
        }
    }

    [Serializable]
    public class SerializedNode : INode, ISerializationCallbackReceiver
    {
        [SerializeReference] private ISerializedObject objectValue;
        [SerializeField] private JSerializedObject serializedObject = new();
        
        public object Value
        {
            get => objectValue;
            set => objectValue = (ISerializedObject) value;
        }

        public void OnBeforeSerialize()
        {
            if(objectValue != null)
                serializedObject.OnBeforeSerialize(objectValue);
        }

        public void OnAfterDeserialize()
        {
            if(objectValue != null)
                serializedObject.OnAfterDeserialize(objectValue);
        }
    }
    
}