﻿using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace InterfaceWrapper.Runtime
{
    [Serializable]
    public class UnityNode : INode
    {
        public object Value
        {
            get => unityObject;
            set => unityObject = (Object)value;
        }

        [SerializeField] private Object unityObject;
    }
}

