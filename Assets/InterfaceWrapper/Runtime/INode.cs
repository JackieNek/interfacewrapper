﻿namespace InterfaceWrapper.Runtime
{
    public interface INode
    {
        object Value { get; set; }
    }
}