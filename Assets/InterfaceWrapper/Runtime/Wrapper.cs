﻿using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace InterfaceWrapper.Runtime
{
    [Serializable]
    public class Wrapper<T>
    {
#if UNITY_EDITOR
        [SerializeField, HideInInspector] private string type;
        [SerializeField, HideInInspector] private string assembly;
#endif
        [SerializeReference, HideInInspector] private INode node;


        public T Value
        {
            get => (T)node?.Value;
            set
            {
                if (value == null)
                {
                    node = null;
                }
                else
                {
                    if (value is Object)
                    {
                        node = new UnityNode();
                    }
                    else
                    {
                        node = new SystemNode();
                    }

                    node.Value = value;
                }
            }
        }

        public static implicit operator T(Wrapper<T> wrapper)
        {
            return wrapper.Value;
        }

        public static implicit operator Wrapper<T>(T value)
        {
            return new Wrapper<T> { Value = value };
        }
    }
}