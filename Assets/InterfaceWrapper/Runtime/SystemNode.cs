﻿using System;
using UnityEngine;

namespace InterfaceWrapper.Runtime
{
    [Serializable]
    public class SystemNode : INode
    {
        [SerializeReference] private object systemObject;

        public object Value
        {
            get => systemObject;
            set => systemObject = value;
        }
    }
}