﻿using System;
using System.Reflection;
using InterfaceWrapper.Runtime;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace InterfaceWrapper.Editor
{
    [CustomPropertyDrawer(typeof(Wrapper<>), true)]
    public class WrapperDrawer : PropertyDrawer
    {
        private Type unityObjectType = typeof(Object);

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            var nodeProperty = property.FindPropertyRelative("node");
            var boxValue = nodeProperty.boxedValue;

            if (boxValue == null || !property.isExpanded)
                return EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;

            if (boxValue is not SystemNode)
                return 2 * (EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing);

            var systemObjectProperty = nodeProperty.FindPropertyRelative("systemObject");
            return base.GetPropertyHeight(systemObjectProperty, label) +
                   (EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing);
        }

        private static Type Interface(int depth, Type type)
        {
            if (depth == 0)
            {
                if (type.IsGenericType)
                {
                    return type.GenericTypeArguments[0];
                }

                if (type.IsArray)
                {
                    return type.GetElementType();
                }
            }
            else
            {
                if (type.IsGenericType)
                {
                    return Interface(depth - 1, type.GenericTypeArguments[0]);
                }

                if (type.IsArray)
                {
                    return Interface(depth - 1, type.GetElementType());
                }
            }

            return null;
        }


        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var typeProperty = property.FindPropertyRelative("type");
            var assemblyProperty = property.FindPropertyRelative("assembly");
            var nodeProperty = property.FindPropertyRelative("node");

            var interfaceType = Interface(property.depth, fieldInfo.FieldType);

            var typeString = typeProperty.stringValue;
            var assemblyString = assemblyProperty.stringValue;
            var type = string.IsNullOrEmpty(assemblyString) || string.IsNullOrEmpty(typeString)
                ? null
                : Assembly.Load(assemblyString).GetType(typeString);

            var children = TypeCache.Children(interfaceType);

            var typeIndex = Mathf.Max(Array.IndexOf(children, type), 0);

            if (type == null)
            {
                typeIndex = EditorGUI.Popup(position, label, typeIndex, GUIContentCache.Children(interfaceType));
            }
            else
            {
                var foldoutPosition = new Rect(position)
                {
                    width = EditorGUIUtility.labelWidth,
                    height = EditorGUIUtility.singleLineHeight
                };

                var popupPosition = new Rect(position)
                {
                    x = position.x + foldoutPosition.width,
                    width = position.width - foldoutPosition.width,
                    height = EditorGUIUtility.singleLineHeight,
                };
                if (unityObjectType.IsAssignableFrom(type))
                {
                    EditorGUI.indentLevel--;
                    property.isExpanded = EditorGUI.Foldout(foldoutPosition, property.isExpanded, label, true);
                    EditorGUI.indentLevel++;
                }

                typeIndex = EditorGUI.Popup(popupPosition, typeIndex, GUIContentCache.Children(interfaceType));
            }


            if (type != children[typeIndex])
            {
                type = children[typeIndex];
                if (type == null)
                {
                    nodeProperty.boxedValue = null;
                    typeProperty.stringValue = null;
                    assemblyProperty.stringValue = null;
                }
                else
                {
                    typeProperty.stringValue = type.ToString();
                    assemblyProperty.stringValue = Assembly.GetAssembly(type).ToString();

                    if (unityObjectType.IsAssignableFrom(type))
                    {
                        nodeProperty.boxedValue = new UnityNode();
                    }
                    else
                    {
                        nodeProperty.boxedValue = new SystemNode
                        {
                            Value = Activator.CreateInstance(type),
                        };
                    }
                }
            }

            if (type != null)
            {
                if (unityObjectType.IsAssignableFrom(type))
                {
                    if (property.isExpanded)
                    {
                        var contentPosition = new Rect(position)
                        {
                            y = position.y + EditorGUIUtility.standardVerticalSpacing +
                                EditorGUIUtility.singleLineHeight,
                            height = position.height - EditorGUIUtility.standardVerticalSpacing -
                                     EditorGUIUtility.singleLineHeight
                        };
                        var unityProperty = nodeProperty.FindPropertyRelative("unityObject");
                        unityProperty.objectReferenceValue = EditorGUI.ObjectField(contentPosition,
                            unityProperty.objectReferenceValue, type, true);
                    }
                }
                else
                {
                    var contentPosition = new Rect(position)
                    {
                        height = position.height - EditorGUIUtility.standardVerticalSpacing -
                                 EditorGUIUtility.singleLineHeight
                    };
                    var systemProperty = nodeProperty.FindPropertyRelative("systemObject");
                    EditorGUI.indentLevel--;
                    EditorGUI.PropertyField(contentPosition, systemProperty, label, true);
                    EditorGUI.indentLevel++;
                    property.isExpanded = systemProperty.isExpanded;
                }
            }
        }
    }
}