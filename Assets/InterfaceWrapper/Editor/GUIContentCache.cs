﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace InterfaceWrapper.Editor
{
    public static class GUIContentCache {
        private static Dictionary<Type, GUIContent[]> map = new();
        public static GUIContent[] Children(Type parent)
        {
            if (map.TryGetValue(parent, out var array))
            {
                return array;
            }

            array = TypeCache.Children(parent).Select(GUIContentByType).ToArray();
            map.Add(parent, array);
            return array;
        }

        private static GUIContent GUIContentByType(Type type)
        {
            if (type == null)
                return new GUIContent("<null>");
        
            return new GUIContent( $"{Assembly.GetAssembly(type).GetName().Name}/{type.Name}");
        }
    }
}

