﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace InterfaceWrapper.Editor
{
    public static class TypeCache
    {
        private static Dictionary<Type, Type[]> typesMap = new();

        public static Type[] Children(Type parent)
        {
            if (typesMap.TryGetValue(parent, out var array))
            {
                return array;
            }

            var list = new List<Type> { null };
            list.AddRange(
                AppDomain.CurrentDomain.GetAssemblies()
                    .SelectMany(assembly => assembly.GetTypes())
                    .Where(t => !t.IsInterface && !t.IsAbstract && !t.IsGenericType)
                    .Where(parent.IsAssignableFrom)
            );
            array = list.ToArray();
            typesMap.Add(parent, array);
            return array;
        }
    }
}